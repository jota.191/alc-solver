
%if False

> {-# LANGUAGE TypeOperators #-}

> module AST where

%endif

< import Control.Monad.Logic

Usaremos la mónada de no determinismo, para hacer programación lógica.
Lo mismo se puede lograr con la mónada de listas. Pero
{\tt Control.Monad.Logic} está optimizado y
exporta el operador \emph{interleave} ({\tt >>-}) que permite evitar en cierta
medida entrar en loops por ramas divergentes (en nuestro caso de todas
formas se prueba que el tableaux siempre termina).
Los roles y los conceptos son simplemente un identificador
\emph{wrappeado}:

> type Name     = String
> newtype Role  = Role Name deriving (Eq,Ord,Show)
> newtype ACon  = ACon Name deriving (Eq,Ord,Show)

Representamos a los conceptos con un tipo inductivo algebraico:

> data Con
>   = Bot
>   | Top
>   | Atom ACon
>   | Neg Con
>   | Con :&&: Con
>   | Con :||: Con
>   | Some Role Con
>   | All  Role Con
>   deriving (Eq, Ord, Show)

%if False


> isAtomic :: Con -> Bool
> isAtomic (Atom _) = True
> isAtomic (Neg (Atom _)) = True
> isAtomic _ = False

%endif
Implementamos la computación de la forma normal de negación
de un concepto:

> nnf :: Con -> Con
> nnf (Atom a)    = Atom a
> nnf (c :||: d)  = nnf c :||: nnf d
> nnf (c :&&: d)  = nnf c :&&: nnf d
> nnf (Some r c)  = Some r ( nnf c)
> nnf (All r c)   = All r ( nnf c)

\newpage\noindent
Los casos que vienen precedidos por una negación son los interesantes:

> nnf (Neg (Atom a))    = Neg $ Atom a
> nnf (Neg (Neg c))     = nnf c
> nnf (Neg (c :||: d))  = nnf (Neg c) :&&: nnf (Neg d)
> nnf (Neg (c :&&: d))  = nnf (Neg c) :||: nnf (Neg d)
> nnf (Neg (Some r c))  = All  r (nnf ( Neg c))
> nnf (Neg (All  r c))  = Some r (nnf ( Neg c))

%if False

> test  = nnf $ Neg $ Some (Role "R") ((Neg (Atom (ACon "A"))) :&&: (Atom (ACon "B")) )
> test2 = (Neg (Atom (ACon "A"))) :&&: (Atom (ACon "A"))
> test3 = Some r a :||: a

> r = Role "R"; s = Role "S"; t = Role "T"
> a = Atom (ACon "A"); b = Atom (ACon "B")
> c = Atom (ACon "C"); d = Atom (ACon "D"); e = Atom (ACon "E");

%endif
