\documentclass{article}


\usepackage{amsmath}
\usepackage{amssymb}

%include polycode.fmt

\title{Implementaci\'on de un razonador}
\date{}

%format :||: = "\vee"
%format :&&: = "\wedge"
%format Top = "\top"
%format Bot = "\bot"
%format Neg = "\neg"

%format Con = "\mathcal{C}"

%format Some (a) (b) = "\exists" a " . \:" b
%format All (a) (b) = "\forall" a " . \:" b

%format >>- = "> \!\! > \!\!\!\! -"

\begin{document}

\maketitle

%include ./AST.lhs
%include ./Reasoner.lhs



\end{document}
