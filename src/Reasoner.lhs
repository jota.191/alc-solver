%if False

> module Reasoner where
> import AST

> import qualified Data.Map as M
> import Data.List
> import Control.Monad.Logic

%endif

Al implementar el razonador haremos énfasis en la
claridad que nos provee el poder de abstracción de un
lenguaje funcional puro como Haskell. No pensamos en la performance,
que será muy mejorable haciendo un poco de transformaci\'on de programas.


El grafo modelo construido en $\mathcal{ALC}$ es de hecho un
árbol. Definamos una estructura de datos para representarle.
Recordemos un vértice tienen asociado un conjunto de roles
$\mathcal{L}(x)$. Para representarle, vamos a ser astutos y en el
propio tipo de datos guardar cierta informaci\'on:
\begin{enumerate}
\item Asociar los conceptos atómicos y sus negaciones para hallar las
ramas inconsistentes
\item Separar qué conceptos (no atómicos) ya se usaron en una regla y cuales
no.
\end{enumerate}
La implementación:

> data Graph
>   = Node {
>     name :: Name,               -- el nombre del nodo
>     chi  :: [([Role], Graph)],  -- nodos adyacentes, etiquetados
>     lbl  :: LCon                -- lista de conceptos (estructurada)
>   } deriving Show

%if False

> initGraph :: Con -> Graph
> initGraph c = Node "x" [] (LCon M.empty [] [c])

%endif
Los conjuntos $\mathcal{L}$ se modelan con la siguiente estructura:

> data LCon
>   = LCon {
>     atoms  :: M.Map Name LitOcc,  -- ocurrencias de literales
>     closed :: [Con],              -- conceptos usados
>     open   :: [Con]               -- conceptos por usar
>   } deriving Show


%if False

> member :: Con -> LCon -> Bool
> member c (LCon _ clo op) = elem c clo || elem c op

> insertCon :: Con -> LCon -> LCon
> insertCon c lc@(LCon ats cl op)
>   | member c lc = lc
>   | otherwise   = LCon ats cl (c : op)  

> insertConGraph :: Con -> Graph -> Graph
> insertConGraph c (Node n ch l) = Node n ch (insertCon c l)

%endif

> data LitOcc = Posi | Nega | Both deriving (Show,Eq)

La función {\tt branch} hace el trabajo difícil. Se elige, si es posible
un concepto no procesado en la raíz del árbol para aplicar una regla.
Si no quedan conceptos a procesar en la raíz,
se procede a hacerlo con los hijos.
Notar que eliminamos el no determinismo generado por decidir qué regla
aplicar eligiendo arbitrariamente.


> branch :: Graph -> [Graph]
> branch node@(Node nam chi l)
>   = case open l of
>       (c : cs) -> rootBranch c (Node nam chi (l {open = cs}))
>       []  -> case childBranch chi of
>         (_ , []    ) -> [node]
>         (cls , ((k,c):opn))
>            -> [Node nam (cls ++ (k,fork):opn ) l | fork <- branch c]

%if False

> isOpen :: Graph -> Bool
> isOpen node@(Node nam chi l)
>   = hasOpenRoot node || any isOpen (map snd chi) 

> hasOpenRoot :: Graph -> Bool 
> hasOpenRoot node = ((/= []) . open . lbl) node

> childBranch :: [([Role], Graph)]
>           -> ([([Role], Graph)],
>               [([Role], Graph)])

> childBranch = break (isOpen . snd)

> inconsistent :: Graph -> Bool
> inconsistent node
>   = M.foldr ((||) .(==Both)) False ((atoms . lbl) node)
>     ||
>    any inconsistent (map snd (chi node))

%endif
La función {\tt rootBranch} se encarga de que, dado un concepto que está
siendo procesado sobre un nodo, generar el(los) nuevo(s) grafo(s).


> rootBranch :: Con -> Graph -> [Graph]

Su implementación es lo más trabajoso, mostramos algunos ejemplos:

> rootBranch (Bot) node = mzero 
> rootBranch (Top) node = branch node

Si tenemos un átomo $A$ positivo y en $\mathcal{L}(x)$ aparecía el mismo
literal pero negativo, cerramos la rama ({\tt mzero}, la computación falla).
De lo contrario generamos la rama con $\mathcal{L}(x) \leftarrow A$

> rootBranch c@(Atom (ACon n)) (node@(Node nam chi (LCon at cl op)))
>   | M.lookup n at == Just Nega
>      = mzero
>   | otherwise
>      = return $ Node nam chi (LCon (M.insert n Posi at) (c : cl) op)

Con el negativo, análogo:

> rootBranch c@(Neg (Atom (ACon n))) (node@(Node nam chi (LCon at cl op)))
>   | M.lookup n at == Just Posi
>      = mzero
>   | otherwise
>      = return $ Node nam chi (LCon (M.insert n Nega at) (c : cl) op)

%if False

> rootBranch (Neg _) _ = error "impossible, not in neg. normal form!"

> rootBranch c@(d :&&: e) (Node nam chi (LCon at cl op))
>   = return $ Node nam chi (LCon at (c : cl) (d : e : op))

%endif
Caso interesante: la disjunción genera dos ramas posibles:

> rootBranch c@(d :||: e) (Node nam chi (LCon at cl op))
>   = return (Node nam chi (LCon at (c : cl) (d : op)))
>     `mplus`
>     return (Node nam chi (LCon at (c : cl) (e : op)))

%if False

> rootBranch (Some r c) (Node nam chi (LCon at cl op))
>   = let rTagged = filter (elem r . fst) chi
>         some    = any (member c) (map (lbl . snd ) rTagged)
>     in if some
>        then branch $ Node nam chi (LCon at (Some r c : cl) op)
>        else branch $ Node nam (([r], newchi) :chi) (LCon at (Some r c : cl) op)
>          where newchi = Node "x" -- hack! todos los nodos tienen
>                                  -- el mismo nombre, por ahora. Hay que arreglar esto si pretendemos extender el razonador
>  
>                              [] (LCon M.empty [] [c])


%endif

\newpage
\noindent
El caso del cuantificador universal: Tomamos los nodos adyacentes etiquetados
por el rol, y les insertamos a todos el concepto en cuestión.

> rootBranch (All r c) (Node nam chi (LCon at cl op))
>   = let  (rs, nonrs) = partition (elem r . fst) chi
>          chi'        = (map (\(r,g) -> (r, insertConGraph c g)) rs) ++ nonrs
>     in branch $ Node nam chi' (LCon at (All r c :cl) op)



La función {\tt tableauxFork} usa {\tt branch} y la mónada de no-determinismo
para implementar la elección no determinista.



> tableauxFork :: Graph -> Logic Graph
> tableauxFork
>   = msum . map return . branch

La función {\tt tableaux}, poda las ramas inconsistentes,
o aplica {\tt tableauxFork} mientras el grafo esté consistente
y haya reglas a aplicar. De lo contrario para.

> tableaux :: Graph -> Logic Graph
> tableaux g
>   | inconsistent g = mzero
>   | isOpen g  = tableauxFork g >>- tableaux
>   | otherwise = return g

Finalmente implementamos la decisión de satisfababilidad. Se corre
el tableaux y se busca una rama buena. La misma va a contener un
modelo canónico que se podría extraer.

> sat c
>   = case observeMany 1 (tableaux (initGraph c)) of
>       [] -> False
>       _  -> True


%if False

> n1 =
>   Node {name = "x",
>         chi = [(undefined,Node "y" [] (LCon undefined undefined [undefined]))],
>         lbl = LCon {atoms = M.fromList [],
>                     closed = [Atom (ACon "A") :||: Atom (ACon "D")],
>                     open = []}}


> nonsat1 = (a :&&: b) :&&: (Neg a) 
> nonsat2 = (a :||: b) :&&: ((Neg a) :&&: Neg b)

%endif
